<?php
session_start();

$page = 'buy.php';

include "connection.php";


function buy() {
	$get = mysqli_query($GLOBALS['connection'], 'SELECT ProductID, ProductName, ProductDescription, ProductPrice, ProductShipping, ProductImage FROM product WHERE ProductQuantity >0 ORDER BY ProductID DESC');
	if (mysqli_num_rows($get) == 0) {
		echo 'Your cart is empty';
	} else {
		while ($get_row = mysqli_fetch_assoc($get)) {
			
			echo '<div class="title">' . $get_row['ProductName'] . '</div>';
			echo '<table width="474" border="2">';
			echo '<tr>';	
			echo '<td><p><font size="2"><strong>Product Name</font></strong></p></td>';
			echo '<td><p>'. $get_row['ProductName'] . '</p></td>';
			echo '<tr>';	
			echo '<td><p><font size="2"><strong>Product Description</font></strong></p></td>';
			echo '<td><p>' . $get_row['ProductDescription'] . '</p></td>';
			echo '<tr>';	
			echo '<td><p><font size="2"><strong>Product Price</font></strong></p></td>';
			echo '<td><p>' . $get_row['ProductPrice'] . '&pound</p></td>';
			echo '<tr>';	
			echo '<td><p><font size="2"><strong>Product Shipping Cost</font></strong></p></td>';
			echo '<td><p>' . $get_row['ProductShipping'] . '&pound</p></td>';
			echo '<tr>';	
			echo '<td><p><font size="2"><strong>Product Image</font></strong></p></td>';
			echo '<td><img src="./images/'. $get_row['ProductImage'].'" /></td>';
			echo '<tr>';
			echo '<td><a href="signin.php"><img src="images/buynow.gif" width="107" height="26" border="0">';
			echo '</table>';
		}
		
	}

}

