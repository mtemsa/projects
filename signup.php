<?php
	//connection to database
	include "connection.php";
	
	if ($_SERVER['REQUEST_METHOD']=='POST'){
	
	//Validation for Username
    $errors = array();
    if(empty($_POST['name'])){
        $errors['name'] = 'Please fill in your Username';//Username
    }else{
		
		  $name = mysqli_real_escape_string($connection,trim($_POST['name']));
		//validation to check if the username posted is in use.
        if($check = $connection->query("SELECT * FROM users WHERE name = '$name'")){
            if($check->num_rows){
                $errors['name'] = 'Username in use';
            }
        }else{
            $errors['name'] = 'the qeury did not work';
        }
		
        
    }
	// validation for address textfield 
    if(empty($_POST['address'])){
        $errors['address'] = 'Please fill in your address';
    }else{
        $address = mysqli_real_escape_string($connection,trim($_POST['address']));
    }
	//validation for phone no 
    if(empty($_POST['phone'])){
        $errors['phone'] = 'Please fill in your phone';
    }else{
        $phone = mysqli_real_escape_string($connection,trim($_POST['phone']));
    }
	
	//validation for sex column to be selected. 
    if(empty($_POST['sex'])){
        $errors['sex'] = 'Please fill in your gender';
    }else{
		
		if( $_POST['sex'] == 'default')
		{
			$errors['sex'] = 'Please select your gender';
		}
		else{	
        $sex = mysqli_real_escape_string($connection,trim($_POST['sex']));
		}
    }
	//validation for email
    if(empty($_POST['email'])){
        $errors['email'] = 'Please fill in your email';
    }else{
        $email = mysqli_real_escape_string($connection,trim($_POST['email']));

        if($check = $connection->query("SELECT * FROM users WHERE email = '$email'")){
            if($check->num_rows){
                $errors['email'] = 'Email in use';
            }
        }else{
            $errors['email'] = 'the qeury did not work';
        }
    }

	//validation for password
    if(empty($_POST['pass1'])){
        $errors['pass1'] = 'Please fill in password';
    }else{
        $pass1 = $_POST['pass1'];
    }
	//checking if the password ented in verify password is matching the password field
    if(empty($_POST['pass2'])){
        $errors['pass2'] = 'Please verify password';
    }else{
        $pass2 = $_POST['pass2'];

        if($pass1!=$pass2){
            $errors ['pass2'] = 'passwords do not match';
        }else{
            $password = mysqli_real_escape_string($connection,trim($_POST['pass1']));
           // $password = sha1($password);
        }
    }
	
    if(empty($errors)){
        $query  = "INSERT INTO users ";
        $query .= "(name,address,phone,sex,email,password,usertype) ";
        $query .= "VALUES ('$name','$address','$phone','$sex','$email','$password','1')";

        $register = $connection->query($query);


$user_id = $connection->query("SELECT user_id FROM users WHERE email = '$email' and name = '$name'")->fetch_object()->user_id;


		$query1  = "INSERT INTO cart ";
        $query1 .= "(user_id) ";
        $query1 .= "VALUES ('$user_id')";

        $addCart = $connection->query($query1);


        if(!$register && !addCart){
        
			echo $query;    
		}
		else
		{
			$message = 'Registration successfully completed, You can now login';
		}
    }
}
?>

<!DOCTYPE html PUBLIC>
<html>
<head>
<title>BASHIRI STORE</title>
<link href="main.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">

function clearText(field){

    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;

}
</script>

<style type="text/css">
<!--
.style6 {font-family: Geneva, Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; color: #9bcdff; }
-->
</style>
</head>
<body>
	<div id="container">
		<div id="header"></div>  
			<div id="content">
				<div id="left_content">
					<div class="menu">
                	<ul>
						<li><a href="index.php" target="_parent">Home</a></li>
						<li><a href="buy.php" target="_parent">Products</a></li>
					  	<li><a href="about_us.php" target="_parent">About Us </a></li>
						<li><a href="adminsignin.php" target="_parent">Admin Page</a></li>
					</ul>
                </div>
            <div class="section">
            	<div class="icon_home">
					<h1>OUR COMPANY LOCATION</h1>
                    <p>
                    	OUR COMPANY'S MAIN OFFICE IS LOCATED IN DUBAI DEIRA,AL MANAL CENTRE GROUND FLOUR SS-01, OUR BRANCH IN MALAYSIA IS LOCATED IN NEGERI SEMBILAN MANTIN, BUILDING NAME TORKANE SIYA, 2ND FLOUR, OFFICE NO 3 
                    .</p>

                </div>
			</div>
            <div class="section_bottom_line"></div>
            <div class="section">
           	  <div class="icon_cube">
           		  <h1>OUR WORKING TIMINGS</h1>
                    <p>MONDAY - FRIDAY </p>
                    <p>  9AM TO 5PM</p>
                  <p> FOR ADDITIONAL INFORMATION PLEASE CALL 017-9919101 OR </p>
                  <p>03-788541225</p>
           	  </div>
			</div>
            <div class="section_bottom_line"></div>
            
            <div class="section">
           	  <div class="icon_tick">
                	<h1>COMPANY FUTURE PLAN</h1>
                    <p>
                    	UPDATE INTO MOBILE PLATFORM 
                    SO MY CUSTOMERS CAN LOGIN AND BUY MY PRODUCTS WITH A MOBILE PHONE THAT HAS AN INTERNET CONNECDTION. </p>
              </div>
			</div>
            </div>
            
			<div id="right_content">
				<div id="content_area">
                	<div class="title">Register Now</div>
					
					   <div style="margin:0 0 0 15px;display: inline-block;padding-top: 10px;width: 95%">
					   <?php 
					    if ($_SERVER['REQUEST_METHOD']=='POST'){ if(empty($errors)){ echo '<span style="color:green;">'.$message; 
							} else 
					    { echo '<span style="font-size: 13px;"><font color="red">OOPPS!!! Something Wrong...</font></span><br><br><span style="
						display: block;
						padding: 10px;
						background-color: #FFE5E5;
						margin-bottom:5px;
						color: #FF5757;">'. implode("</span><span style='
						display: block;
						padding: 10px;
						background-color: #FFE5E5;
						margin-bottom:5px;
						color: #FF5757;'>",$errors). '</span>';} } 
						?>
					   </div>	
					   
					<form action="signup.php" method="post" name="Register" class="form_input">
				    <input id="name" type="text" name="name" placeholder="Username" style="width:45.5%;">
					<br />
					<input id="pass1" type="password" name="pass1" placeholder="Password" style="width:45.5%;">
					<br />
					<input id="pass2" type="password" name="pass2" placeholder="Confirm Password" style="width:45.5%;">
					<br />	
					<input id="address" type="text" name="address" placeholder="Valid Address" style="
					width: 95%;"><br>
					<input id="phone" name="phone" placeholder="Phone Number" type="text" style="width:45.5%;">        
					<br />
					<select name="sex" style="width: 49%; visibility: visible;">
					<option value="default">Choose your gender</option>
					<option value="Male">Male</option>
					<option value="Female">Female</option>
					</select><br />
					<input id="email" type="email" name="email" placeholder="Email=Example@email.com" style="width:45.5%;">
					<br />
					
					<input type="submit" name="submit" value="Submit"> 
					</form> 
                    
					<a href="./signin.php">To Login Click here</a>;
                      <div class="title"></div>
                    <div class="thumbs">	                
			      </div>
       	      </div>
             <div id="right_content_bottom">
           Copyright � 2015 BASHIRI STORE, ALL RIGHTS RESERVED, BASHIRI TISSOT GROUP(U.A.E)lnc. </div>
        </div>
    </div>
</html>