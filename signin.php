<!DOCTYPE html PUBLIC>
<html>
<head>
<title>BASHIRI STORE</title>
<link href="main.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">
function clearText(field){

    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;

}
</script>

<style type="text/css">
<!--
.style6 {font-family: Geneva, Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; color: #9bcdff; }
-->
</style>
</head>
<body>

	<?php
	//connection to database
	include "connection.php";
	//start a session
	session_start();
	?>
		
	<div id="container">
		<div id="header">
		<div id="login_form">
				<section id="signin">
					<?php
					if(!isset($_SESSION['name']))
					{
					?>
				<?php
						if (isset($_SESSION['errors'])){
					}
					}
					else
					{
						echo '<font color="yellow">Welcome To BASHIRI store ' . $_SESSION['name'] . '</font><br />';
						echo '<a href="./logout.php">logout</a>';
					}
					?>
            </div>
		</div>  
        <div id="content">
        	<div id="left_content">
            	<div class="menu">
                	<ul>
						<li><a href="index.php" target="_parent">Home</a></li>
						<li><a href="buy.php" target="_parent">Products</a></li>
					  	<li><a href="about_us.php" target="_parent">About Us </a></li>
						<li><a href="adminsignin.php" target="_parent">Admin Page</a></li>
					</ul>
                </div>
            <div class="section">
            	<div class="icon_home">
					<h1>OUR COMPANY LOCATION</h1>
                    <p>
						Our Company's main Office is located in Dubai Deira, Al manal Centre, Ground flour SS-01, Our other Branch is in Malaysia and is located in Negeri sembilan, Mantin, Buildin name is torkane Siya, 2nd Flour, Office No 3
                    .</p>

                </div>
			</div>
            <div class="section_bottom_line"></div>
            <div class="section">
           	  <div class="icon_cube">
           		  <h1>OUR WORKING TIMINGS</h1>
                    <p>Monday - Friday </p>
                    <p> 9am To 5pm </p>
                    <p> For additional information please call 017-9919101 OR </p>
                  <p>03-788541225</p>
           	  </div>
			</div>
            <div class="section_bottom_line"></div>
            
            <div class="section">
            	<div class="icon_tick">
                	<h1>COMPANY FUTURE PLAN</h1>
                    <p>
					Update into Mobile Platform so all users can login and buy Bashiri Store 
					Products with a mobile phone that has an internet connection </p>
                </div>
			</div>
            </div>
            
			<div id="right_content">
				<div id="content_area">
                	<div class="title"><font color="yellow">Please Sign in To continue Shopping </font></div>
					<p>If you are a member of bashiri store then you can sign in to continue. </p>
					<div class="title">Sign In</div>
					<section id="signin">
					<?php
					if(!isset($_SESSION['name']))
					{
					?>
				  <form method="post" action="./login.php">	
					<table width="200" border="0">
					  <tr>
						<td><label for="username" class="style6">Username: </label></td>
						<td><input type="text" name="name" value=''/></td>
					  </tr>
					  <tr>
						<td><label for="password" class="style6">Password: </label></td>
						<td><input type="password" name="password" /></td>
					  </tr>
					  <tr>
					    <td>&nbsp;</td>
					    <td><button type="submit">Login</button></td>
				      </tr>
					</table>
				  </form>
				
					<?php
						if (isset($_SESSION['errors'])){
					}
					}
					else
					{
						echo '<a href="./product.php">To Continue shopping Click here</a>';
					}
					?>
                    
				
				
                      <div class="title">If you are not a member <a href="Signup.php">CLICK HERE</a> to Register </div>
                      <div class="thumbs">	                
			      </div>
       	      </div>
             <div id="right_content_bottom">
           Copyright � 2015 BASHIRI STORE, ALL RIGHTS RESERVED, BASHIRI TISSOT GROUP(U.A.E)lnc. </div>
        </div>
    </div>
</html>