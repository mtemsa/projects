<!DOCTYPE html PUBLIC>
<html>
<head>
<title>BASHIRI STORE</title>
<link href="main.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">
function clearText(field){

    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;

}
</script>

</head>
<body>
	<?php
	//make connection to database
	include "connection.php";
	//start a session
	session_start();
	?>
	<div id="container">
		<div id="header">
        	<div id="login_form">
				<section id="signin">
					<?php
					if(!isset($_SESSION['name']))
					{
					?>
					
            	<form method="post" action="./login.php">
                    <label>Login:</label>
                	  <input name="name" value="username" type="text" onFocus="clearText(this)" onBlur="clearText(this)" class="textfield"/>
                      <input name="password" value="password" type="password" onFocus="clearText(this)" onBlur="clearText(this)" class="textfield"/>
                	  <input type="submit" name="submit" value="" class="button"/>
               	</form>
				<?php
						if (isset($_SESSION['errors'])){
					}
					}
					else
					{
						echo '<font color="yellow">Welcome To BASHIRI store ' . $_SESSION['name'] . '</font><br />';
						echo '<a href="./logout.php">logout</a>';
					}
					?>
            </div>
        </div>
        <div id="content">
        	<div id="left_content">
            	<div class="menu">
                	<ul>
						<li><a href="index.php" target="_parent">Home</a></li>
						<li><a href="buy.php" target="_parent">Products</a></li>
					  	<li><a href="about_us.php" target="_parent" class="current">About Us</a></li>
						<li><a href="adminsignin.php" target="_parent">Admin Page</a></li>
					</ul>
                </div>

			<div class="section_bottom_line"></div>
            <div class="section">
            	<div class="icon_home">
					<h1>OUR COMPANY LOCATION</h1>
                    <p>
                    	OUR COMPANY'S MAIN OFFICE IS LOCATED IN DUBAI DEIRA,AL MANAL CENTRE GROUND FLOUR SS-01, OUR BRANCH IN MALAYSIA IS LOCATED IN NEGERI SEMBILAN MANTIN, BUILDING NAME TORKANE SIYA, 2ND FLOUR, OFFICE NO 3 
                    .</p>

                </div>
			</div>
            <div class="section_bottom_line"></div>
            <div class="section">
           	  <div class="icon_cube">
           		  <h1>OUR WORKING TIMINGS</h1>
                    <p>MONDAY - FRIDAY </p>
                    <p>  9AM TO 5PM</p>
                  <p> FOR ADDITIONAL INFORMATION PLEASE CALL 017-9919101 OR </p>
                  <p>03-788541225</p>
           	  </div>
			</div>
            <div class="section_bottom_line"></div>
            
            <div class="section">
           	  <div class="icon_tick">
                	<h1>COMPANY FUTURE PLAN</h1>
                    <p>
                    	UPDATE INTO MOBILE PLATFORM 
                    SO MY CUSTOMERS CAN LOGIN AND BUY MY PRODUCTS WITH A MOBILE PHONE THAT HAS AN INTERNET CONNECDTION. </p>
              </div>
			</div>
            <div class="section_bottom_line"></div>
            
            </div><!-- End Of left Content -->
            <div id="right_content">
				<div id="content_area">
                	<div class="title">ABOUT US</div>
					<p>BASHIRI STORE IS LOCATED IN DUBAI, AS YOU CAN SEE, ALL THE INFO IS ON THE LEFT SIDE BAR OF THIS WEBSITE IN EVERY
					PAGE, THIS WEBSITE IS VERY SHORT AND BRIEF, AND MOST IMPORTANTLY IT IS VERY USER FRIENDLY. SO FEEL FREE TO <a href="signup.php">JOIN US</a>.</p>

					<div class="title">ABOUT THE PAYMENT</div>
                    <div class="thumbs">
                    	<a title="Products" href="products.php" target="_parent"><img src="images/watch1.jpg" alt="Latest Product" width="480" height="200" longdesc="index.html"></a>
                  <div class="clear_with_height"></div>
                  </div>
				  <br />
				  <p>
                  THE PAYMENT METHOD USED FOR THIS WEBSITE IS PAYPAL. WE APOLOGIZE IF PAYPAL ITS NOT YOUR CHOICE, AT THE MOMENT
				  THE ONLY PAYMENT METHOD AVAILABLE IS PAYPAL, HOW EVER IN SHORT FUTURE WE WILL UPGRADE OUR PAYMENT METHODS. FOR 
				  ANY SUGGESTIONS YOU CAN CONTACT US USING THE QUICK CONTACT EMAIL BELOW.
				  </p>
                    
              <div class="title">
                    	QUICK CONTACT
                  </div>
                    <p>
                   	Tel: +60-17-9919101 <br />
               	  	Mobile: +60-14-2244322
					</p>
                    <p>
					Email: mtemsa@yahoo.com
					</p>
			  </div>
            </div><!-- End Of Right Content -->
            <div id="right_content_bottom">
           	Copyright © 2015 BASHIRI STORE, ALL RIGHTS RESERVED, BASHIRI TISSOT GROUP(U.A.E)lnc.</div>
        </div><!-- End Of Content -->
    </div><!-- End Of Container -->
</html>